# To Do App

### Create, Read, Delete Task in One Second

<br>

### Web Technology I Use

<img src="https://raw.githubusercontent.com/biswas-abhishek/biswas-abhishek/18ee2fa0ef68e42cab7611df750f99d040ee62f9/img/html.svg" alt='HTML' Title="html">
<img src='https://raw.githubusercontent.com/biswas-abhishek/biswas-abhishek/18ee2fa0ef68e42cab7611df750f99d040ee62f9/img/css.svg' alt='css' title="CSS" />
<img src='https://raw.githubusercontent.com/biswas-abhishek/biswas-abhishek/18ee2fa0ef68e42cab7611df750f99d040ee62f9/img/Vue.svg' alt="Vue.js" title="Vue.js" />

---

Level : Beginner

---

Preview Link: <a href="todov1.netlify.app" target="_blank">Click Here </a>

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
